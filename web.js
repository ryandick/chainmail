var express = require( 'express' );
var email = require( "emailjs" );
var app = express.createServer();
var unicorn = require( 'unicorn' );
var fs = require( 'fs' );
var path = require( 'path' );
var Mustache = require( 'mustache' );
unicorn.install();
var server = email.server.connect( {
	user: "drecksteinsa@gmail.com",
	password: "4natalie2013",
	host: "smtp.gmail.com",
	ssl: true
} );
app.use( express.bodyParser() );
app.use( express.logger() );
app.post( '/send', function ( req, res ) {
	res.header( 'Access-Control-Allow-Origin', '*' );
	res.header( 'Access-Control-Allow-Methods', 'POST' );
	res.header( 'Access-Control-Allow-Headers', 'Content-Type' );
	var m = "SENDING |>	".green().bold() + ' received request, body: ' + JSON.stringify( req.body );
	console.log( m );
	if ( req.body.template ) {
		var pathName = path.resolve( __dirname, './templates/' + req.body.template );
		var template = fs.readFileSync( pathName );
		var html = Mustache.to_html( template.toString(), {
			data: req.body.templateData
		}, null );
		req.body.text = html.toString('utf8');
	}
	if ( req.body.text ) {
		// send the message and get a callback with an error or details of the message that was sent
		server.send( {
			text: req.body.text,
			from: req.body.name + ' <' + req.body.from + '>',
			to: req.body.to,
			subject: req.body.subject
		}, function ( err, message ) {
			if ( err ) {
				console.log( err );
				res.json( {
					hasErr: true,
					err: 'Failed to send',
					error: err,
					message: message
				} );
			} else {
				var m = "SENT |>	".green().bold() + ' received request, body: ' + JSON.stringify( req.body );
				console.log( m );
				res.json( {
					result: 'message sent',
					message: message
				} );
			}
		} );
	} else {
		res.json( {
			hasErr: true,
			error: 'No text to send'
		} );
	}
} );
var port = process.env.PORT || 5000;
app.listen( port, function () {
	console.log( "Listening on " + port );
} );
